# author: 左志华
# date: 2022-07-30
#
# 用于持续集成测试 (CI testing)
# @note 运行脚本：`./test/ci.ps1`
Write-Output "Copyright (c) 2022 左志华(zuo.zhihua@qq.com)"
Write-Output "*** 建立 To-SPH 的 fpm 测试 ..."
Write-Host "$(Get-Date)"

fpm test
build\gfortran_E6887F0AEE32E4FC\app\To-SPH --help
build\gfortran_E6887F0AEE32E4FC\app\To-SPH --version
build\gfortran_E6887F0AEE32E4FC\app\To-SPH pre -C "example" --skip
# build\gfortran_E6887F0AEE32E4FC\app\To-SPH post -C "example" # @todo

Write-Output "*** To-SPH 的 fpm 测试完成！"
