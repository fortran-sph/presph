-- dofile("luanames.lua")
-- x, vx, mass, rho, p, u, itype, hsml, nreal = input()
-- 初始化2维剪切腔体的流体实粒子
function region()
    local x = {} -- loc
    local vx = {} -- vel
    local mass = {} -- mass
    local rho = {} -- rho
    local p = {} -- pressure
    local u = {} -- internal energy
    local itype = {} -- type

    local m = 41
    local n = 41
    local mp = m - 1
    local np = n - 1
    local nreal = mp * np
    local ntotal = nreal

    local xl = 1.0e-3
    local yl = 1.0e-3
    local dx = xl / mp -- hsml
    local dy = yl / np

    -- 初始化多维数组
    x[1] = {}
    x[2] = {}
    vx[1] = {}
    vx[2] = {}

    for i = 1, mp, 1 do
        for j = 1, np, 1 do
            local k = j + (i - 1) * np
            x[1][k] = (i - 1) * dx + dx / 2
            x[2][k] = (j - 1) * dy + dy / 2
        end
    end

    local c = 10 * math.sqrt(2 * 9.8 * 1.0e-3)
    for i = 1, nreal, 1 do
        vx[1][i] = 0.0
        vx[2][i] = 0.0
        p[i] = 1000.0 * 9.8 * (1.0e-3 - x[2][i])
        rho[i] = p[i] / (c * c) + 1000.0
        mass[i] = dx * dy * rho[i]
        u[i] = 357.1
        itype[i] = 2 -- 淡水
    end

    -- 初始化虚粒子
    -- 下边界
    for i = 1, 6 * mp + 1 do
        ntotal = ntotal + 1
        x[1][ntotal] = (i - 1) * dx / 2 - xl
        x[2][ntotal] = 0.0 -- dx / 2 * (j - 1)
        vx[1][ntotal] = 0.0
        vx[2][ntotal] = 0.0
    end

    -- 左边界
    for i = 1, 2 * mp - 1 do
        -- for j = 1, 3, 1 do
        ntotal = ntotal + 1
        x[1][ntotal] = 0.0 - xl -- dx / 2 * (j - 1)
        x[2][ntotal] = i * dx / 2
        vx[1][ntotal] = 0.0
        vx[2][ntotal] = 0.0
        -- end
    end

    -- 右边界
    for i = 1, 2 * mp - 1 do
        -- for j = 1, 3, 1 do
        ntotal = ntotal + 1
        x[1][ntotal] = xl * 2 -- + dx / 2 * (j - 1)
        x[2][ntotal] = i * dx / 2
        vx[1][ntotal] = 0.0
        vx[2][ntotal] = 0.0
        -- end
    end

    for i = nreal, ntotal do
        p[i] = 1000.0 * 9.8 * (1.0e-3 - x[2][i])
        rho[i] = p[i] / (c * c) + 1000.0
        mass[i] = rho[i] * dx * dx
        u[i] = 357.1
        itype[i] = -2 -- 虚粒子淡水
    end

    return x, vx, mass, rho, p, u, itype, dx, nreal, ntotal - nreal

end

